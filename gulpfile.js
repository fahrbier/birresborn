const { src, dest } = require('gulp');
const concat = require('gulp-concat');

function defaultTask(cb) {
  src('input/**/*.js')
     .pipe(dest('output/', { overwrite: true }));  
  cb();
}

function mergeServices(cb) {
    
    return src([
        './src/configs/start.inc',
        './src/configs/application_birresborn.de.inc',
        './src/libraries/tools/get_params.inc',
        './src/libraries/tools/parse_string.inc',
        './src/libraries/database/getDataSqlServer.inc',
        './src/libraries/tools/print.inc',
        './src/services/events/search_list.inc',  
        './src/services/events/descriptor.inc', 
        './src/services/news/search_list.inc',  
        './src/services/pictures/search_list.inc',  
        './src/services/pictures/flickr/search_list.inc',    
        './src/services/adverts/search_list.inc',                                  
        './src/configs/end.inc',              
    ]).pipe(concat('/mpp-dist.asp'))
    .pipe(dest('./serve/services/'));
    
    cb();
}

exports.default = defaultTask
exports.mergeServices = mergeServices
